from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.SimpleRouter()
router.register(r'voice-channels', views.VoiceChannelViewset)
router.register(r'root-voice-channels', views.RootVoiceChannelDupViewset)
router.register(r'voice-channels-duplicates', views.VoiceChannelDupViewset)
router.register(r'guilds', views.GuildViewset)
router.register(r'guild-member', views.GuildMemberViewset)
router.register(r'bug', views.BugViewset)


urlpatterns = [
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
