from django.contrib import admin
from bot_api.models import *

# Register your models here.
admin.site.register(GuildModel)
admin.site.register(GuildMember)
admin.site.register(VoiceChannelDuplicate)
admin.site.register(RootVoiceChannelDuplication)
admin.site.register(VoiceChannel)
