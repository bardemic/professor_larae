
from django.http import HttpResponse
import threading
from rest_framework import viewsets
from bot_api.serialzers import *
from rest_framework.response import Response
from django.shortcuts import get_object_or_404


# def index(request):
#     t = threading.Thread(target=startup)
#     t.start()
#     return HttpResponse("Starting Bot")


class VoiceChannelViewset(viewsets.ModelViewSet):
    """API endpoint for Streamer VoiceChannels to be viewed and edited"""
    queryset = VoiceChannel.objects.all().order_by('id')
    serializer_class = VoiceChannelSerializer
    lookup_field = 'member_id'


class RootVoiceChannelDupViewset(viewsets.ModelViewSet):
    """API endpoint for the RootVoiceChannels to be viewed and edited"""
    queryset = RootVoiceChannelDuplication.objects.all().order_by('id')
    serializer_class = RootVoiceChannelDupSerializer
    lookup_field = 'channel_id'


class VoiceChannelDupViewset(viewsets.ModelViewSet):
    """API endpoint for Duplicated VoiceChannels to be viewed and edited"""
    serializer_class = VoiceChannelDupSerializer
    lookup_field = 'channel_id'
    queryset = VoiceChannelDuplicate.objects.all().order_by('root_channel', 'channel_number', 'id')

    def get_queryset(self):
        queryset = VoiceChannelDuplicate.objects.all().order_by('root_channel', 'channel_number', 'id')
        root_id = self.request.query_params.get('root_id', None)
        if root_id is not None:
            queryset = queryset.filter(root_channel__channel_id=root_id)
            print('test')
        return queryset


class GuildViewset(viewsets.ModelViewSet):
    """API endpoint for Guilds to be viewed and edited"""
    queryset = GuildModel.objects.all().order_by('pk')
    serializer_class = GuildSerializer
    lookup_field = 'guild_id'


class GuildMemberViewset(viewsets.ModelViewSet):
    """API endpoint for Guild Members to be viewed and edited"""
    queryset = GuildMember.objects.all().order_by('guild')
    serializer_class = GuildMemberSerializer
    lookup_field = 'member_id'


class BugViewset(viewsets.ModelViewSet):
    queryset = BugModel.objects.all().order_by('found')
    serializer_class = BugSerializer
