from django.db import models

# private voice channel model
class VoiceChannel(models.Model):
    channel_id = models.IntegerField(unique=True)
    member_id = models.IntegerField()
    role_id = models.IntegerField()


class RootVoiceChannelDuplication(models.Model):
    guild_id = models.BigIntegerField()
    channel_id = models.BigIntegerField(unique=True)
    channel_name = models.TextField()


class VoiceChannelDuplicate(models.Model):
    guild_id = models.BigIntegerField()
    channel_id = models.BigIntegerField(unique=True)
    channel_number = models.BigIntegerField()
    root_channel = models.ForeignKey(RootVoiceChannelDuplication, on_delete=models.DO_NOTHING)


class GuildModel(models.Model):
    guild_id = models.BigIntegerField(unique=True)
    private_role_id = models.BigIntegerField(default=0)
    max_warns = models.BigIntegerField(default=2)
    kick_on_max_warns = models.BooleanField(default=False)
    admin_channel = models.BigIntegerField(default=0)
    dm_on_warn = models.BooleanField(default=True)
    private_manage_role_id = models.BigIntegerField(default=0)


#TODO fix member_id needs to be unique. Warned in more than 1 server
class GuildMember(models.Model):
    guild = models.ForeignKey(GuildModel, on_delete=models.DO_NOTHING, related_name='members')
    member_id = models.BigIntegerField(null=False, unique=True)
    warns = models.BigIntegerField(null=False, default=0)
    last_warn = models.DateField(auto_now=True)


class BugModel(models.Model):
    disc = models.TextField(null=False)
    member_id = models.BigIntegerField(null=False)
    found = models.DateField(auto_now=True)
