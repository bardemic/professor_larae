from rest_framework import serializers
from rest_framework.response import Response

from bot_api.models import *


class VoiceChannelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VoiceChannel
        fields = ['id', 'channel_id', 'member_id', 'role_id']


class RootVoiceChannelDupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RootVoiceChannelDuplication
        fields = ['id', 'guild_id', 'channel_id', 'channel_name']


class VoiceChannelDupSerializer(serializers.HyperlinkedModelSerializer):
    root_channel = serializers.PrimaryKeyRelatedField(many=False, required=True, queryset=RootVoiceChannelDuplication.objects.all())
    #root_channel = serializers.HyperlinkedRelatedField(
        #many=False, required=True, queryset=RootVoiceChannelDuplication.objects.all(), view_name='root-voice-channels', lookup_field='channel_id')
    class Meta:
        model = VoiceChannelDuplicate
        fields = ['id', 'guild_id', 'channel_id', 'channel_number', 'root_channel']



class GuildSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GuildModel
        fields = ['id', 'guild_id', 'private_role_id', 'max_warns', 'kick_on_max_warns', 'admin_channel', 'dm_on_warn', 'private_manage_role_id']


class GuildMemberSerializer(serializers.HyperlinkedModelSerializer):
    guild = serializers.PrimaryKeyRelatedField(many=False, required=True, queryset=GuildModel.objects.all())

    class Meta:
        model = GuildMember
        fields = ['id', 'guild', 'member_id', 'warns', 'last_warn']


class BugSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BugModel
        fields = ['disc', 'member_id', 'found']
