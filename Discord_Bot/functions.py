import concurrent.futures
import requests
import json

url_ip = 'http://192.168.1.147'
port = ':8000'

def store_privateroom(member, role, channel):
    r = requests.post(url_ip+port+'/bot/api/voice-channels/', data={'member_id': member, 'role_id': role, 'channel_id': channel})



def get_privateroom(member):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_privateroom_helper, member)
        db_obj = future.result()
        if db_obj.get('detail', False):
            return None
        return (db_obj['member_id'], db_obj['role_id'], db_obj['channel_id'])


def get_privateroom_helper(member):
    r = requests.get(url_ip + port + '/bot/api/voice-channels/' + str(member))
    return json.loads(r.text)


def delete_privateroom(member):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(delete_privateroom_helper, member)


def delete_privateroom_helper(member):
    r = requests.delete(url_ip + port + '/bot/api/voice-channels/' + str(member))
    return r.status_code


def get_privaterooms():
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_privaterooms_helper)
        return future.result()


def get_privaterooms_helper():
    r = requests.get(url_ip + port + '/bot/api/voice-channels/')
    return json.loads(r.text)


def has_root(channel_id):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(has_root_helper, channel_id)
        return future.result()


def has_root_helper(channel_id):
    r = requests.get(url_ip + port + '/bot/api/root-voice-channels/' + str(channel_id))
    data = json.loads(r.text)
    if data.get('detail', False):
        r = requests.get(url_ip + port + '/bot/api/voice-channels-duplicates/' + str(channel_id))
        data = json.loads(r.text)
        if data.get('root_id', False):
            return True
        else:
            return False
    return True


def get_root(channel_id):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_root_helper, channel_id)
        return future.result()


def get_root_helper(channel_id):
    r = requests.get(url_ip + port + '/bot/api/root-voice-channels/' + str(channel_id))
    data = json.loads(r.text)
    if data.get('detail', False):
        r = requests.get(url_ip + port + '/bot/api/voice-channels-duplicates/' + str(channel_id))
        data = json.loads(r.text)
        if data.get('root_id', False):
            return data
        else:
            return None
    return data


def create_root(guild_id, channel):
    r = requests.post(url_ip + port + '/bot/api/root-voice-channels/', data=
        {'guild_id': guild_id, 'channel_id': channel.id, 'channel_name': channel.name})
    return r.status_code


def delete_root(channel_id):
    r = requests.delete(url_ip + port + '/bot/api/root-voice-channels/'+ str(channel_id))
    return r.status_code


def create_duplicate(cloned_channel, root, channel_number):
    root_dict = get_root_helper(root)
    r = requests.post(url_ip + port + '/bot/api/voice-channels-duplicates/', data=
    {'channel_id': cloned_channel.id, 'guild_id': cloned_channel.guild.id, 'channel_number': channel_number, 'root_channel': root_dict['id']})
    return r.status_code


def is_root(channel_id):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(is_root_helper, channel_id)
        return future.result()


def is_root_helper(channel_id):
    r = requests.get(url_ip + port + '/bot/api/root-voice-channels/'+ str(channel_id))
    data = json.loads(r.text)
    if data.get('detail', False):
        return False
    return True



def get_duplicate_channels(root):
    #with concurrent.futures.ThreadPoolExecutor() as executor:
        #future = executor.submit(get_duplicate_channels_helper, root)
        #return future.result()
        return get_duplicate_channels_helper(root)


#TODO fix this????
def get_duplicate_channels_helper(root):
    r = requests.get(url_ip + port + '/bot/api/voice-channels-duplicates/?root_id='+str(root.id))
    channels =json.loads(r.text)
    empty = 0
    for channel in channels:
        discord_channel = root.guild.get_channel(channel['channel_id'])
        if discord_channel is not None and len(discord_channel.members) < 2:
            empty += 1
    if len(root.members) < 2:
        empty += 1
    return empty



def delete_duplicate(channel_id):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(delete_duplicate_helper, channel_id)
        return future.result()


def delete_duplicate_helper(channel_id):
    r = requests.delete(url_ip + port + '/bot/api/voice-channels-duplicates/'+str(channel_id))
    return r.status_code


def get_empty_duplicate(root_id, guild):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_empty_duplicate_helper, root_id, guild)
        return future.result()


def get_empty_duplicate_helper(root, guild):
    r = requests.get(url_ip + port + '/bot/api/voice-channels-duplicates/?root_id=' + str(root.id))
    channels = json.loads(r.text)
    for channel in channels:
        discord_channel = guild.get_channel(channel['channel_id'])
        if discord_channel is not None and len(discord_channel.members) == 0:
            return discord_channel
        elif discord_channel is None:
            delete_duplicate(channel['channel_id'])
    return None


def list_roots():
    r = requests.get(url_ip + port + '/bot/api/root-voice-channels/')
    return json.loads(r.text)


def create_guild(guild_id):
    r = requests.post(url_ip + port + '/bot/api/guilds/', data={'guild_id': guild_id})
    return r.status_code


def delete_guild(guild_id):
    r = requests.delete(url_ip + port + '/bot/api/guilds/'+str(guild_id))
    return r.status_code


def set_guild_role(guild_id, role_id):
    r = requests.post(url_ip + port + '/bot/api/guilds/', data={'guild_id': guild_id, 'role_id': role_id})
    return r.status_code


def get_guild_role(guild_id):
    r = requests.get(url_ip + port + '/bot/api/guilds/'+str(guild_id))
    return json.loads(r.text).get('private_role_id', None)


def set_guild_admin_channel(guild_id, channel_id):
    r = requests.post(url_ip + port + '/bot/api/guilds/', data={'guild_id': guild_id, 'admin_channel': channel_id})
    return r.status_code


def get_guild_admin_channel(guild_id):
    r = requests.get(url_ip + port + '/bot/api/guilds/' + str(guild_id))
    return json.loads(r.text).get('admin_channel', None)


def set_max_warns(guild_id, max_warns):
    r = requests.put(url_ip + port + '/bot/api/guilds/'+ str(guild_id)+'/', data={'guild_id': guild_id, 'max_warns': max_warns})
    return r.status_code


def get_max_warns(guild_id):
    r = requests.get(url_ip + port + '/bot/api/guilds/' + str(guild_id))
    data = json.loads(r.text)
    return data.get('max_warns', None), data.get('kick_on_max_warns', None)


def set_dm_on_warn(guild_id, d):
    r = requests.post(url_ip + port + '/bot/api/guilds/', data={'guild_id': guild_id, 'dm_on_warn': d})
    return r.status_code


def set_kick_on_max_warns(guild_id, kick):
    r = requests.post(url_ip + port + '/bot/api/guilds/', data={'guild_id': guild_id, 'kick_on_max_warns': kick})
    return r.status_code


def warn_guild_member(guild_id, member_id):
    r = requests.get(url_ip + port + '/bot/api/guild-member/' + str(member_id))
    data = json.loads(r.text)
    g = requests.get(url_ip + port + '/bot/api/guilds/' + str(guild_id))
    gdata = json.loads(g.text)
    if data.get('detail', False):
        r = requests.post(url_ip + port + '/bot/api/guild-member/', data={
            'guild': gdata['id'], 'member_id': member_id, 'warns': 1}
        )
    else:
        r = requests.put(url_ip + port + '/bot/api/guild-member/'+ str(member_id)+'/', data={
            'guild': gdata['id'], 'member_id': member_id, 'warns': data.get('warns', 1)+ 1})
    r = requests.get(url_ip + port + '/bot/api/guild-member/' + str(member_id))
    data = json.loads(r.text)
    return data.get('last_warn', None), data.get('warns', None)


def set_guild_member_warns(guild_id, member_id, warns):
    r = requests.get(url_ip + port + '/bot/api/guild-member/' + str(member_id))
    data = json.loads(r.text)
    g = requests.get(url_ip + port + '/bot/api/guilds/' + str(guild_id))
    gdata = json.loads(g.text)
    if data.get('detail', False):
        r = requests.post(url_ip + port + '/bot/api/guild-member/', data={
            'guild': gdata['id'], 'member_id': member_id, 'warns': warns}
                          )
    else:
        r = requests.put(url_ip + port + '/bot/api/guild-member/'+ str(member_id)+'/', data=
            {'guild': gdata['id'], 'member_id': member_id, 'warns': warns})
    r = requests.get(url_ip + port + '/bot/api/guild-member/' + str(member_id))
    data = json.loads(r.text)
    return data.get('last_warn', 'Error'), data.get('warns', 'Unknown')


#TODO Only works if I fix the view
def get_last_warn(guild_id, member_id):
    r = requests.get(url_ip + port + '/bot/api/guild-member/' + str(member_id))
    data = json.loads(r.text)
    return data.get('last_warn', None)


def create_bug(disc, member_id):
    r = requests.post(url_ip+port+'/bot/api/bug/', data={'disc': disc, 'member_id': member_id})
    data = json.loads(r.text)
    return data


def set_private_manage_role(guild, role_id):
    pass


def get_private_manage_role(guild):
    pass
