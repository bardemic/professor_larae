import asyncio
from discord.ext import commands
import discord

from Discord_Bot.functions import *

asyncio.set_event_loop(asyncio.new_event_loop())
bot = commands.Bot(command_prefix="!", case_insensitive=True, help_command=None)
port = '8000'

def get_category(name, guild):
    for category in guild.categories:
        if category.name.lower() == name.lower():
            return category
    return None


def has_role(member, role_name):
    for role in member.roles:
        if role.name.lower() == role_name.lower():
            return True
    return False


async def has_private_role(ctx):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_guild_role, ctx.guild.id)
        role_id = future.result()
    if role_id != 0:
        role = ctx.guild.get_role(role_id)
        if not (role in ctx.author.roles):
            await ctx.send("You need to have the " + role.name + " role")
            return False
    return True


async def purge_streaming_channels(guild):
    for privateRoom in get_privaterooms():
        role = guild.get_role(privateRoom['role_id'])
        channel = guild.get_channel(privateRoom['channel_id'])
        delete_privateroom(privateRoom['member_id'])
        # delete the role and channel
        await role.delete(reason='Deleted by purge command')
        await channel.delete(reason='Deleted by purge command')


async def private_check_admin_or_role(ctx):
    pass


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='bug')
async def found_bug(ctx, description: str, *args):
    """Submits a bug report with the given description"""
    if len(args)>0:
        description = description +' '+ str(' '.join(args))
    author = ctx.author.id
    create_bug(description, author)
    await ctx.send('Submitted the bug')


@bot.group(name='development')
async def trello_link(ctx):
    """Sends link of trello board of known bugs and upcoming features"""
    await ctx.send('https://trello.com/b/2nCVA0ce/professor-larae')


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='dmonwarn')
async def dm_on_warn(ctx, boolean: bool):
    """Sets if the bot will dm members when using !warn"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(set_dm_on_warn, ctx.guild.id, boolean)
        future.result()
    if boolean:
        await ctx.send('I will now dm members when they are warned')
    else:
        await ctx.send('I will not dm members when they are warned')


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='setadminchannel', aliases=['setadmin'])
async def set_admin_channel(ctx, channel: discord.TextChannel):
    """Sets the channel warnings will happen in"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(set_guild_admin_channel, ctx.guild.id, channel.id)
        future.result()
    await ctx.send('Set ' + channel.name + ' as the admin channel')


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='warn')
async def warn_member(ctx, member: discord.Member, reason: str):
    """Warns a member"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(warn_guild_member, ctx.guild.id, member.id)
        last_warn, warns = future.result()
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_guild_admin_channel, ctx.guild.id)
        admin_channel = future.result()
    if admin_channel != 0:
        await ctx.guild.get_channel(admin_channel).send(
            'Warned ' + member.display_name + '. They were last warned ' + str(last_warn) + ' and they have ' + str(
                warns) + ' warns.')
    else:
        await ctx.send(
            'Warned ' + member.display_name + '. They were last warned ' + str(last_warn) + ' and they have ' + str(
                warns) + ' warns.')
    await member.send('You were just warned by ' + ctx.author.display_name+' in '+ctx.guild.name + '. Reason: ' +reason)
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_max_warns, ctx.guild.id)
        max_warns, kick_on_max_warns = future.result()
    if kick_on_max_warns and warns >= max_warns:
        await member.kick(reason='Too many warns')
        if admin_channel != 0:
            await ctx.guild.get_channel(admin_channel).send(
                'Kicked ' + member.display_name + ' for having too many warns')
        else:
            await ctx.send('Kicked ' + member.display_name + ' for having too many warns')


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='resetwarns')
async def reset_warns_member(ctx, member: discord.Member):
    """Resets a member's warns to 0"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(set_guild_member_warns, ctx.guild.id, member.id, 0)
        last_warn, warns = future.result()
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_guild_admin_channel, ctx.guild.id)
        admin_channel = future.result()
    if admin_channel != 0:
        await ctx.guild.get_channel(admin_channel).send('Set ' + member.display_name + ' warnings to 0.')
    else:
        await ctx.send('Set ' + member.display_name + ' warnings to 0.')


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='setwarns')
async def set_warns_member(ctx, member: discord.Member, warns: int):
    """Sets a member's warns to the given value"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(set_guild_member_warns, ctx.guild.id, member.id, warns)
        last_warn, warns = future.result()
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_guild_admin_channel, ctx.guild.id)
        admin_channel = future.result()
    if admin_channel != 0:
        await ctx.guild.get_channel(admin_channel).send('Set ' + member.display_name + ' warnings to ' + str(warns) + '.')
    else:
        await ctx.send('Set ' + member.display_name + ' warnings to ' + str(warns) + '.')


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='setmaxwarns')
async def set_max_warns_command(ctx, max_warns: int):
    """Sets the max warning a member can have"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(set_max_warns, ctx.guild.id, max_warns)
        future.result()
    await ctx.send('Set max warns to ' + str(max_warns))


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='kickonmaxwarns', aliases=['kick_on_max_warns', 'kickonwarns'])
async def kick_on_max_warns(ctx, boolean: bool):
    """Sets if the bot will kick when a member reaches the max warnings"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(set_kick_on_max_warns, ctx.guild.id, boolean)
        max_warns = future.result()
    if boolean:
        await ctx.send('Will now kick on ' + str(max_warns) + ' warns.')
    else:
        await ctx.send("Won't kick on warns.")


@commands.guild_only()
@commands.has_permissions(administrator=True)
@bot.group(hidden=True, name='getmaxwarns', aliases=['maxwarns'])
async def max_warns(ctx):
    """Gets the max warns of the guild"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(get_max_warns, ctx.guild.id)
        mwarns, kick = future.result()
    if kick:
        await ctx.send('Will now kick on ' + str(mwarns) + ' warns.')
    else:
        await ctx.send("Won't kick on warns. Max warns set to " + str(mwarns))


@bot.group(name='privateroom', aliases=['private'])
@commands.guild_only()
async def private_base_command(ctx):
    """Creates and Manages a private voice channel for streamers"""
    if ctx.invoked_subcommand is None:
        await ctx.send('Invalid privateroom command passed.')


@private_base_command.command(hidden=True, name='setrole')
@commands.has_permissions(administrator=True)
async def private_set_role(ctx, role: discord.Role):
    """sets the required role for privateroom command. Default: None"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(set_guild_role, ctx.guild.id, role.id)
        future.result()
    await ctx.send("Set " + role.name + " as the required role for the privateroom commands")


@private_base_command.command(hidden=True, name='removerole')
@commands.has_permissions(administrator=True)
async def private_remove_role(ctx):
    """Removes the required role for privateroom command"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(set_guild_role, ctx.guild.id, 0)
        future.result()
    await ctx.send("Removing the role to access the privateroom commands")


@private_base_command.command(name='create')
@commands.check(has_private_role)
async def private_create(ctx):
    """Creates a private voice channel for streamers"""
    # check if the author is streaming
    if type(ctx.author.activity) is discord.activity.Streaming or has_role(ctx.author, 'admin'):
        privateChannel = get_privateroom(ctx.author.id)
        # check if the member has a channel already
        if not privateChannel:
            #TODO make this set through command
            cat = get_category('private streaming', ctx.guild)
            if not cat:
                cat = await ctx.message.guild.create_category_channel('private streaming', overwrites=None,
                                                                      reason='Professor Larae found that it was missing')
            # creates the role and channel
            name = ctx.author.display_name
            privateRole = await ctx.guild.create_role(name=name, reason="Created via command",
                                                      mentionable=False)
            overwrites = {
                privateRole: discord.PermissionOverwrite(connect=True, speak=True),
                ctx.guild.default_role: discord.PermissionOverwrite(connect=False, speak=False)
            }
            privateVoice = await cat.create_voice_channel(name + '\'s channel', overwrites=overwrites)
            await ctx.author.add_roles(privateRole, reason="Created via command")
            store_privateroom(ctx.author.id, privateRole.id, privateVoice.id)
            await ctx.send('Created a channel for you.')
        else:
            await ctx.send('You already have a channel. Dummy')

    else:
        await ctx.send('You must be streaming for this feature')


@private_base_command.command(hidden=True, name='purge')
@commands.has_permissions(administrator=True)
async def purge_streaming_channels_command(ctx):
    """Deletes all private voice channels and roles"""
    await purge_streaming_channels(ctx.guild)
    await ctx.send('Deleting all streaming channels.')


@private_base_command.command(name='invite')
async def private_invite(ctx, member: discord.Member):
    """Gives mentioned member a role to join the channel"""

    # member, role, channel
    privateChannel = get_privateroom(ctx.author.id)
    # check if the member has a channel
    if privateChannel:
        role = ctx.guild.get_role(privateChannel[1])
        # give the mentioned users the role
        await member.add_roles(role, reason=str(ctx.author) + ' invited them')
        await ctx.send("" + member.display_name + " can now join your channel.")
    else:
        await ctx.send('You don\'t have a channel.')


@private_base_command.command(name='kick')
async def private_kick(ctx, member: discord.Member):
    """Removes the mentioned member from the channel"""

    # member, role, channel
    privateChannel = get_privateroom(ctx.author.id)
    # check if the member has a channel
    if privateChannel:
        role = ctx.guild.get_role(privateChannel[1])
        # remove the role and move to afk if they are in the channel
        await member.remove_roles(role, reason=str(ctx.author) + ' kicked them')
        await member.move_to(None, reason=str(ctx.author) + ' kicked them')
        await ctx.send('Kicked ' + member.display_name + ' from your channel.')

    else:
        await ctx.send('You don\'t have a channel.')


@private_base_command.command(name='delete')
async def private_delete(ctx):
    """Delete your private channel"""

    # member, role, channel
    privateChannel = get_privateroom(ctx.author.id)
    # check if the member has a channel
    if privateChannel:
        role = ctx.guild.get_role(privateChannel[1])
        channel = ctx.guild.get_channel(privateChannel[2])
        delete_privateroom(ctx.author.id)
        # delete the role and channel
        await role.delete(reason="Deleted through command")
        await channel.delete(reason="Deleted through command")
        await ctx.send('Deleted your channel.')
    else:
        await ctx.send('You don\'t have a channel.')


@private_base_command.command(hidden=True, name='superdelete')
@commands.has_permissions(administrator=True)
async def private_super_delete(ctx, member: discord.Member):
    """Delete member's private channel"""

    # member, role, channel
    privateChannel = get_privateroom(member.id)
    # check if the member has a channel
    if privateChannel:
        role = ctx.guild.get_role(privateChannel[1])
        channel = ctx.guild.get_channel(privateChannel[2])
        delete_privateroom(member.id)
        # delete the role and channel
        await role.delete(reason="Deleted through command")
        await channel.delete(reason="Deleted through command")
        await ctx.send('Deleted ' + member.display_name + '\'s channel')
    else:
        await ctx.send('They don\'t have a channel.')


def command_help(command, **kwargs):
    # check if the command was a sub command
    if kwargs.get('basecommand', None):
        help = f"`{bot.command_prefix}{kwargs['basecommand'].name} {command.name} "
    else:
        help = f"`{bot.command_prefix}{command.name} "
    for alias in sorted(command.aliases):
        help = help + f"or {bot.command_prefix}{alias} "
    if command.signature != "":
        help = help + f"{command.signature} "
    help = help + f"` - {command.help}"
    return help


@bot.command(aliases=['help'])
async def h(ctx):
    """Display this help message."""
    reply = "I understand the following commands:\n\n"
    for command in sorted(bot.commands, key=lambda x: x.name):
        # dirty way of handling the sub commands
        if command.name == 'privateroom':
            for subcommand in sorted(private_base_command.commands, key=lambda x: x.name):
                if not subcommand.hidden:
                    reply = reply + command_help(subcommand, basecommand=command) + "\n"
        elif not command.hidden:
            reply = reply + command_help(command) + "\n"
    await ctx.send(reply)


@bot.command(hidden=False, aliases=['adminhelp', 'superhelp'])
@commands.has_permissions(administrator=True)
@commands.guild_only()
async def admin_help(ctx):
    """Display this help message for admins."""
    reply = "Oh shit it's an Admin, everyone pretend like you aren't fucking shit up!\n\n"
    for command in sorted(bot.commands, key=lambda x: x.name):
        if command.name == 'privateroom':
            for subcommand in sorted(private_base_command.commands, key=lambda x: x.name):
                if subcommand.hidden:
                    reply = reply + command_help(subcommand, basecommand=command) + "\n"
        elif command.hidden:
            reply = reply + command_help(command) + "\n"
    await ctx.send(reply)
    return


@bot.command(hidden=True, name='setduplicate')
@commands.guild_only()
@commands.has_permissions(administrator=True)
async def set_duplicate_channel(ctx, channel: discord.VoiceChannel):
    """Set a channel to make duplicates when people join. Needs channel id if the name has spaces"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(create_root, ctx.guild.id, channel)
        future.result()
    await ctx.send(channel.name + ' can now be duplicated. (' + str(channel.id) + ')')


@bot.command(hidden=True, name='removeduplicate')
@commands.guild_only()
@commands.has_permissions(administrator=True)
async def delete_duplicate_channel(ctx, channel: discord.VoiceChannel):
    """Will stop a channel from being duplicated when people join. Needs channel id if the name has spaces"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(delete_root, channel.id)
        future.result()
    await ctx.send(channel.name + ' won\'t be duplicated. (' + str(channel.id) + ')')


@bot.command(hidden=True, name='listduplicates')
@commands.guild_only()
@commands.has_permissions(administrator=True)
async def list_duplicate_roots(ctx):
    """Lists all the channels can have temporary duplicates made"""
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(list_roots)
        roots = future.result()
    channels = ''
    for root in roots:
        channels += root['channel_name'] + ', '
    await ctx.send('The channels that can be duplicated are: ' + channels)


@bot.event
async def on_voice_state_update(member, before, after):
    if after.channel:
        # creates a voice channel
        if has_root(after.channel.id):
            root = get_root(after.channel.id)
            root_channel = after.channel.guild.get_channel(root['channel_id'])
            if len(after.channel.members) >= 2 and get_duplicate_channels(root_channel) < 1:
                # get the root channel
                # clone the root and save to db
                cloned_channel = await root_channel.clone(name=root['channel_name'] + ' (temp)')
                position = root_channel.position
                try:
                    await cloned_channel.edit(position=position)
                except:
                    print('Error with position')
                # TODO Get channel numbers working
                with concurrent.futures.ThreadPoolExecutor() as executor:
                    future = executor.submit(create_duplicate, cloned_channel, root['channel_id'], -1)
                    future.result()
    if before.channel:
        # deletes a voice channels
        if not is_root(before.channel.id) and has_root(before.channel.id):
            root = get_root(before.channel.id)
            root = before.channel.guild.get_channel(root['channel_id'])
            duplicates = get_duplicate_channels(root)
            if duplicates > 1:
                delete_duplicate(before.channel.id)
                await before.channel.delete()
        if is_root(before.channel.id):
            duplicates = get_duplicate_channels(before.channel)
            if duplicates > 1:
                channel = get_empty_duplicate(before.channel, before.channel.guild)
                delete_duplicate(channel.id)
                await channel.delete()


@bot.event
async def on_guild_join(guild):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(create_guild, guild.id)
        future.result()


@bot.event
async def on_guild_remove(guild):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(delete_guild, guild.id)
        future.result()


@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, discord.ext.commands.MissingRequiredArgument):
        await ctx.send('Missing argument '+ error.param.name)
    elif isinstance(error, discord.ext.commands.NoPrivateMessage):
        await ctx.send('That command does not work in private messages. Dummy')
    elif isinstance(error, discord.ext.commands.MissingPermissions):
        await ctx.send('Missing permission(s) ' + ' '.join(error.missing_perms))
    else:
        await  ctx.send('There was an error with that command. If this is a bug please notify my creator')


@bot.event
async def on_connect():
    await bot.change_presence(activity=discord.Game(name='with magic'))


@bot.event
async def on_disconnect():
    await bot.connect(reconnect=True)


async def is_owner(ctx):
    return ctx.author.id == 139171653480349697


@bot.command(hidden=True)
@commands.check(is_owner)
async def die(ctx):
    """Kill my currently running instance. I won't forget this."""
    reply = "Rip me"
    await ctx.send(reply)
    await bot.close()
    return


def startup():
    token = get_token()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    bot.run(token)

def get_token():
    # Get the discord bot token from environ
    with open('secrets.json') as json_file:
        data = json.load(json_file)
        token = data['token']
    return token
